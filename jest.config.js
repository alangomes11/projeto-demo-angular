module.exports = {
  preset: "jest-preset-angular",
  setupFilesAfterEnv: [
    "<rootDir>/src/setupJest.ts"
  ],
  snapshotResolver: './snapshot-resolver.js',
  moduleNameMapper: {
    '@app/(.*)': '<rootDir>/src/app/$1',
    '@env/(.*)': '<rootDir>/src/environments/$1',
    '@test/(.*)': '<rootDir>/src/test/$1',
  },
  globals: {
    'ts-jest': {
      diagnostics: false,
      tsConfig: './tsconfig.spec.json',
    }
  },
};
