const path = require('path')

const FOLDER_NAME = '__snapshots__';

module.exports = {
  resolveSnapshotPath: (testPath, snapshotExtension) =>
    testPath.replace('src/app', FOLDER_NAME + '/src/app') + snapshotExtension,

  resolveTestPath: (snapshotFilePath, snapshotExtension) =>
    snapshotFilePath.replace(FOLDER_NAME + '/src/app', 'src/app').slice(0, -snapshotExtension.length),

  testPathForConsistencyCheck: 'src/app/example.test.js',
};
