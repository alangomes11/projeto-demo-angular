import 'zone.js/dist/zone-error';

export const environment = {
  endpoint: 'https://jsonplaceholder.typicode.com',
  production: false
};

