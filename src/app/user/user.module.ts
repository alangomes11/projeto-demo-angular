import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared/shared.module';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { UserEffects, userFeatureKey, userReducers } from './store';
import { UserDetailsComponent } from './user-details/user-details.component';
import { UserRoutingModule } from './user-routing.module';
import { UserResolver } from './user.resolver';


@NgModule({
  declarations: [UserDetailsComponent],
  imports: [
    CommonModule,
    SharedModule,
    UserRoutingModule,
    EffectsModule.forFeature([UserEffects]),
    StoreModule.forFeature(userFeatureKey, userReducers),
  ],
  providers: [
    UserResolver,
  ]
})
export class UserModule { }
