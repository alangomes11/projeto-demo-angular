import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { environment } from '@env/environment';
import { USER_MOCK } from '@test/post.mock';

import { UserService } from './user.service';


describe('UserService', () => {
  let httpMock: HttpTestingController;
  let service: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserService],
    });
    httpMock = TestBed.get(HttpTestingController);
    service = TestBed.get(UserService);
  });

  afterEach(() => httpMock.verify());

  test('deve consultar usuário pelo id', done => {
    service.getUser(5).subscribe(res => {
      expect(res).toEqual(USER_MOCK);
      done();
    });

    httpMock
      .expectOne(`${environment.endpoint}/users/5`)
      .flush(USER_MOCK);
  });
});
