import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { requestPosts, selectPosts } from '@app/post/store';
import { AppState } from '@app/store';
import { select, Store } from '@ngrx/store';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { Observable } from 'rxjs';
import { pluck } from 'rxjs/operators';

import { User } from '../model/user.model';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserDetailsComponent implements OnInit, OnDestroy {

  user$: Observable<User> = this.activatedRoute.data.pipe(pluck('user'));
  posts$ = this.store.pipe(select(selectPosts));

  constructor(
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,
  ) { }

  ngOnInit() {
    this.user$
      .pipe(untilDestroyed(this))
      .subscribe(user => this.store.dispatch(requestPosts({ userId: user.id })));
  }

  ngOnDestroy() { }

}
