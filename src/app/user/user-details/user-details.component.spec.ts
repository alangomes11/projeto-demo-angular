import { TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { requestPosts, selectPosts } from '@app/post/store';
import { PostComponent } from '@app/shared/post/post.component';
import { AppState } from '@app/store';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { POST_MOCK, USER_MOCK } from '@test/post.mock';
import { hot } from 'jest-marbles';
import { of } from 'rxjs';
import { Shallow } from 'shallow-render';
import { Rendering } from 'shallow-render/dist/lib/models/rendering';

import { UserModule } from '../user.module';
import { UserDetailsComponent } from './user-details.component';

describe('UserDetailsComponent', () => {
  let render: Rendering<UserDetailsComponent, never>;
  let store: MockStore<AppState>;

  beforeEach(async () => {
    const shallow = new Shallow(UserDetailsComponent, UserModule)
      .provideMock([
        {
          provide: ActivatedRoute,
          useFactory: () => ({ data: of({ user: USER_MOCK }) })
        },
        provideMockStore({
          selectors: [
            {
              selector: selectPosts,
              value: { result: [POST_MOCK], loading: false },
            }
          ]
        })
      ]);

    render = await shallow.render();
    store = TestBed.get(Store);
  });

  test('deve renderizar os detalhes do usuário', () => {
    render.fixture.detectChanges();

    expect(render.fixture).toMatchSnapshot();
  });

  test('deve listar as postagens do usuário', () => {
    render.fixture.detectChanges();

    expect(store.scannedActions$).toBeObservable(hot('a', { a: requestPosts({ userId: USER_MOCK.id }) }));
    const postComponent = render.findComponent(PostComponent)[0];
    expect(postComponent.post).toEqual(POST_MOCK);
  });
});
