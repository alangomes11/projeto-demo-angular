import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { AppState } from '@app/store';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';

import { User } from './model/user.model';
import { requestUser, selectUser } from './store';


@Injectable()
export class UserResolver implements Resolve<User> {

  constructor(private store: Store<AppState>) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User> {
    const userId = Number(route.paramMap.get('id'));
    this.store.dispatch(requestUser({ id: userId, ignoreCache: true }));

    return this.store
      .pipe(
        select(selectUser, userId),
        first(res => res && !!res.result),
        map(({ result }) => result),
      );
  }

}
