import { TestBed } from '@angular/core/testing';
import { AppState } from '@app/store';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { USER_MOCK } from '@test/post.mock';
import { cold, hot } from 'jest-marbles';

import { requestUser, selectUser } from './store';
import { UserResolver } from './user.resolver';


describe('UserResolver', () => {
  let userResolver: UserResolver;
  let store: MockStore<AppState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        provideMockStore({
          selectors: [
            {
              selector: selectUser,
              value: { result: USER_MOCK }
            }
          ]
        }),
        UserResolver,
      ]
    });

    userResolver = TestBed.get(UserResolver);
    store = TestBed.get(Store);
  });

  test('deve disparar requisição para dados do usuário sem cache', () => {
    const user$ = userResolver.resolve({ paramMap: new Map().set('id', '5') } as any, null);

    expect(store.scannedActions$).toBeObservable(hot('a', {
      a: requestUser({ id: 5, ignoreCache: true }),
    }));
    expect(user$).toBeObservable(cold('(a|)', {
      a: USER_MOCK,
    }));
  });

});
