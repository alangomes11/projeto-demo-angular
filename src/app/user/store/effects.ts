import { Injectable } from '@angular/core';
import { AppState } from '@app/store';
import { showRequestError } from '@app/store/actions';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { forkJoin, of } from 'rxjs';
import { catchError, distinct, filter, first, map, mergeMap } from 'rxjs/operators';

import { UserService } from '../user.service';
import { loadUser, requestUser } from './actions';
import { selectUser } from './selectors';
import { userFeatureKey } from './state';

@Injectable()
export class UserEffects {

  requestUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(requestUser),
      mergeMap((props) => forkJoin(
        of(props),
        this.store.pipe(select(selectUser, props.id), first())
      )),
      filter(([{ ignoreCache }, user]) => ignoreCache || !user || !user.result),
      distinct(([{ id }]) => id, this.actions$.pipe(ofType(requestUser), filter(({ ignoreCache }) => ignoreCache))),
      mergeMap(([{ id }]) =>
        this.userService.getUser(id).pipe(
          map(user => loadUser({ user })),
          catchError(error => of(showRequestError({ feature: userFeatureKey, field: `users.${id}`, error })))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private userService: UserService,
  ) { }

}
