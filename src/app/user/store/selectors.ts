import { createSelector } from '@ngrx/store';

import { AppUserState, selectUserFeature } from './state';

export const selectUser = createSelector(selectUserFeature, (state: AppUserState, id: number) => state.users[id]);
