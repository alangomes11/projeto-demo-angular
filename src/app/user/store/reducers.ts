import { Action, createReducer, on } from '@ngrx/store';

import { loadUser, requestUser } from './actions';
import { AppUserState, userInitialState } from './state';

export const reducers = createReducer(
  userInitialState,
  on(requestUser, (state, { id, ignoreCache }) => ({
    ...state,
    users: {
      ...state.users,
      ...(ignoreCache || !state.users[id] || !state.users[id].result ? { [id]: { loading: true } } : {}),
    }
  })),
  on(loadUser, (state, { user }) => ({
    ...state,
    users: {
      ...state.users,
      [user.id]: { result: user, loading: false },
    }
  })),
);

export function userReducers(state: AppUserState | undefined, action: Action) {
  return reducers(state, action);
}
