import { USER_MOCK } from '@test/post.mock';

import { loadUser, requestUser } from './actions';
import { userReducers } from './reducers';

describe('User reducers', () => {

  const initialState = {
    prop: 1,
    users: {
      1: { result: {}, loading: false },
      5: { result: {}, loading: false },
    }
  };

  test('requestUser action deve limpar dados se não houver', () => {
    const initialStateLimpo = {
      prop: 1,
      users: {
        1: { result: {}, loading: false },
        5: { loading: false },
      }
    };
    const reduced = userReducers(initialStateLimpo as any, requestUser({ id: 5, ignoreCache: false }));

    expect(reduced).toEqual({
      prop: 1,
      users: {
        1: { result: {}, loading: false },
        5: { loading: true },
      }
    });
  });

  test('requestUser action deve manter dados se não ignorar cache', () => {
    const reduced = userReducers(initialState as any, requestUser({ id: 5, ignoreCache: false }));

    expect(reduced).toEqual({
      prop: 1,
      users: {
        1: { result: {}, loading: false },
        5: { result: {}, loading: false },
      }
    });
  });

  test('requestUser action deve limpar dados se ignorar cache', () => {
    const reduced = userReducers(initialState as any, requestUser({ id: 5, ignoreCache: true }));

    expect(reduced).toEqual({
      prop: 1,
      users: {
        1: { result: {}, loading: false },
        5: { loading: true },
      }
    });
  });

  test('loadUser action deve popular dados do usuário', () => {
    const reduced = userReducers(initialState as any, loadUser({ user: USER_MOCK }));

    expect(reduced).toEqual({
      prop: 1,
      users: {
        1: { result: {}, loading: false },
        5: { result: USER_MOCK, loading: false },
      }
    });
  });


});
