import { AsyncResult } from 'src/app/model/http.model';
import { AppState } from 'src/app/store';

import { User } from '../model/user.model';

export const userFeatureKey = 'user';

export const selectUserFeature = (state: AppState): AppUserState =>
  (state[userFeatureKey] as any) as AppUserState;

export interface AppUserState {
  users: {
    [key: number]: AsyncResult<User>
  };
}

export const userInitialState: AppUserState = {
  users: {},
};
