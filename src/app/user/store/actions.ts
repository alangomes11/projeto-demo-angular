import { createAction, props } from '@ngrx/store';

import { User } from '../model/user.model';

export const requestUser = createAction(
  '[Users] Request user',
  props<{ id: number; ignoreCache?: boolean }>(),
);

export const loadUser = createAction(
  '[Users] Load user',
  props<{ user: User }>(),
);
