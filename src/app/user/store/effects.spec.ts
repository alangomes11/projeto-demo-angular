import { TestBed } from '@angular/core/testing';
import { AppState } from '@app/store';
import { showRequestError } from '@app/store/actions';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action, Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { USER_MOCK } from '@test/post.mock';
import { cold, hot } from 'jest-marbles';
import { ReplaySubject } from 'rxjs';

import { UserService } from '../user.service';
import { loadUser, requestUser } from './actions';
import { UserEffects } from './effects';
import { selectUser } from './selectors';
import { userFeatureKey } from './state';


describe('UserEffects', () => {

  const actions = new ReplaySubject<Action>(1);
  const userService: UserService = {} as any;

  let userEffects: UserEffects;
  let store: MockStore<AppState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        provideMockActions(() => actions.asObservable()),
        provideMockStore(),
        UserEffects,
        {
          provide: UserService,
          useFactory: () => userService,
        },
      ]
    });
    userService.getUser = jasmine.createSpy('getUser').and.returnValue(cold('--a|', { a: USER_MOCK }));

    userEffects = TestBed.get(UserEffects);
    store = TestBed.get(Store);
  });

  describe('requestUser$', () => {

    describe('ignoreCache = false', () => {

      test('não deve requisitar caso o usuário ja tenha sido carregado', () => {
        store.overrideSelector(selectUser, { result: USER_MOCK, loading: false });

        actions.next(requestUser({ id: 5, ignoreCache: false }));

        expect(userEffects.requestUser$).toBeObservable(hot('--'));
      });

      test('deve requisitar caso o usuário não tenha sido carregado', () => {
        store.overrideSelector(selectUser, { loading: false });

        actions.next(requestUser({ id: 5, ignoreCache: false }));

        expect(userEffects.requestUser$).toBeObservable(hot('--a', {
          a: loadUser({ user: USER_MOCK }),
        }));
        expect(userService.getUser).toHaveBeenCalledWith(5);
      });

    });

    describe('ignoreCache = true', () => {

      test('deve requisitar mesmo o usuário ja tenha sido carregado', () => {
        store.overrideSelector(selectUser, { result: USER_MOCK, loading: false });

        actions.next(requestUser({ id: 6, ignoreCache: true }));

        expect(userEffects.requestUser$).toBeObservable(hot('--a', {
          a: loadUser({ user: USER_MOCK }),
        }));
        expect(userService.getUser).toHaveBeenCalledWith(6);
      });

    });

    test('deve informar erro na requisição', () => {
      store.overrideSelector(selectUser, { loading: false });
      userService.getUser = jasmine.createSpy('getUser').and.returnValue(cold('--#|'));

      actions.next(requestUser({ id: 5, ignoreCache: true }));

      expect(userEffects.requestUser$).toBeObservable(hot('--a', {
        a: showRequestError({ feature: userFeatureKey, field: 'users.5', error: expect.anything() })
      }));
    });

  });

});
