import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserDetailsComponent } from './user-details/user-details.component';
import { UserResolver } from './user.resolver';


const routes: Routes = [
  {
    path: ':id',
    component: UserDetailsComponent,
    resolve: {
      user: UserResolver,
    },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
