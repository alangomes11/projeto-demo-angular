import {
  BrowserAnimationsModule,
  NoopAnimationsModule,
} from '@angular/platform-browser/animations';
import { POST_MOCK, USER_MOCK } from '@test/post.mock';
import { Shallow } from 'shallow-render';

import { SharedModule } from '../shared.module';
import { PostComponent } from './post.component';


describe('PostComponent', () => {
  let shallow: Shallow<PostComponent>;

  beforeEach(() => {
    shallow = new Shallow(PostComponent, SharedModule)
      .replaceModule(BrowserAnimationsModule, NoopAnimationsModule);
  });

  test('componente deve estar igual ao snapshot', async () => {
    const { fixture } = await shallow.render({ bind: { post: POST_MOCK, user: USER_MOCK }});
    expect(fixture).toMatchSnapshot();
  });
});

