import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Post } from '@app/post/model/post.model';
import { User } from '@app/user/model/user.model';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PostComponent implements OnInit {

  @Input()
  post: Post;

  @Input()
  user: User;

  constructor() { }

  ngOnInit() {
  }

}
