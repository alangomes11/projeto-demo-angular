import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PostComponent } from './post/post.component';



@NgModule({
  declarations: [PostComponent],
  imports: [
    CommonModule,
    RouterModule,
  ],
  exports: [PostComponent],
})
export class SharedModule { }
