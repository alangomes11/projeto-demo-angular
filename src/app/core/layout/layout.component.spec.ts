import { Shallow } from 'shallow-render';

import { CoreModule } from '../core.module';
import { LayoutComponent } from './layout.component';

describe('LayoutComponent', () => {
  let shallow: Shallow<LayoutComponent>;

  beforeEach(() => {
    shallow = new Shallow(LayoutComponent, CoreModule);
  });

  test('componente deve estar igual ao snapshot', async () => {
    const { fixture } = await shallow.render();
    expect(fixture).toMatchSnapshot();
  });
});

