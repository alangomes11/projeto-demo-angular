export interface AsyncResult<T> {
  result?: T;
  loading: boolean;
  error?: any;
}
