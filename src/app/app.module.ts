import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';

import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { metaReducers, reducers } from './store';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    HttpClientModule,
    LoadingBarRouterModule,
    StoreModule.forRoot(reducers, {
      metaReducers,
      runtimeChecks: environment.production ? {
        strictStateImmutability: true,
        strictStateSerializability: true,
      } : {}
    }),
    BrowserAnimationsModule,
    EffectsModule.forRoot([]),
    ...(!environment.production ? [StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production })] : [])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
