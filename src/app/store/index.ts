import { Action, ActionReducerMap, MetaReducer } from '@ngrx/store';
import * as immutable from 'object-path-immutable';

import { AsyncResult } from '../model/http.model';
import { ActionRequestErrorProps, showRequestError } from './actions';

// tslint:disable-next-line: no-empty-interface
export interface AppState {}

export const reducers: ActionReducerMap<AppState> = {

};


export function metaReducer(reducer: any) {
  return (state: AppState, action: Action): AppState => {
    if (action.type === showRequestError.type) {
      const props: ActionRequestErrorProps = action as any;
      if (props.feature && props.field) {
        state = immutable.set(state, `${props.feature}.${props.field}`, { result: null, loading: false } as AsyncResult<
          any
        >);
      }
    }
    return reducer(state, action);
  };
}

export const metaReducers: MetaReducer<AppState>[] = [metaReducer];
