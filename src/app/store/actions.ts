import { createAction, props } from '@ngrx/store';

export interface ActionRequestErrorProps {
  feature?: string;
  field?: string;
  error: any;
  ignoreToastr?: boolean;
}

export const showRequestError = createAction(
  '[App] Show request error',
  props<ActionRequestErrorProps>()
);
