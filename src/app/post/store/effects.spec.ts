import { TestBed } from '@angular/core/testing';
import { AppState } from '@app/store';
import { showRequestError } from '@app/store/actions';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action, Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { POST_MOCK } from '@test/post.mock';
import { cold, hot } from 'jest-marbles';
import { ReplaySubject } from 'rxjs';

import { PostService } from '../post.service';
import { loadPosts, requestPosts } from './actions';
import { PostEffects } from './effects';
import { postFeatureKey } from './state';


describe('PostEffects', () => {

  const actions = new ReplaySubject<Action>(1);
  const postService: PostService = {} as any;

  let postEffects: PostEffects;
  let store: MockStore<AppState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        provideMockActions(() => actions.asObservable()),
        provideMockStore(),
        PostEffects,
        {
          provide: PostService,
          useFactory: () => postService,
        },
      ]
    });
    postService.getPosts = jasmine.createSpy('getPosts').and.returnValue(cold('--a|', { a: [POST_MOCK] }));

    postEffects = TestBed.get(PostEffects);
    store = TestBed.get(Store);
  });

  describe('requestPosts$', () => {

    test('deve requisitar posts e disparar ação para carregar', () => {
      actions.next(requestPosts({}));

      expect(postEffects.requestPosts$).toBeObservable(hot('--a', {
        a: loadPosts({ posts: [POST_MOCK] })
      }));
    });

    test('deve informar erro na requisição', () => {
      postService.getPosts = jasmine.createSpy('getPosts').and.returnValue(cold('--#|'));

      actions.next(requestPosts({}));

      expect(postEffects.requestPosts$).toBeObservable(hot('--a', {
        a: showRequestError({ feature: postFeatureKey, field: 'posts', error: expect.anything() })
      }));
    });

  });

});
