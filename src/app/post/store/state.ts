import { AsyncResult } from 'src/app/model/http.model';
import { AppState } from 'src/app/store';

import { Post } from '../model/post.model';

export const postFeatureKey = 'post';

export const selectPostFeature = (state: AppState): AppPostState =>
  (state[postFeatureKey] as any) as AppPostState;


export interface AppPostState {
  posts: AsyncResult<Post[]>;
}

export const postInitialState: AppPostState = {
  posts: { loading: false },
};
