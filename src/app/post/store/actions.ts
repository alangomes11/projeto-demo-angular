import { createAction, props } from '@ngrx/store';

import { Post } from '../model/post.model';

export const requestPosts = createAction(
  '[Posts] Request posts',
  props<{ userId?: number }>()
);

export const loadPosts = createAction(
  '[Posts] Load posts',
  props<{ posts: Post[] }>(),
);
