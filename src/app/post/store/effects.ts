import { Injectable } from '@angular/core';
import { AppState } from '@app/store';
import { showRequestError } from '@app/store/actions';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, exhaustMap, map } from 'rxjs/operators';

import { PostService } from '../post.service';
import { loadPosts, requestPosts } from './actions';
import { postFeatureKey } from './state';

@Injectable()
export class PostEffects {

  requestPosts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(requestPosts),
      exhaustMap(({ userId }) =>
        this.postService.getPosts({ userId }).pipe(
          map(posts => loadPosts({ posts })),
          catchError(error => of(showRequestError({ feature: postFeatureKey, field: 'posts', error })))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private postService: PostService,
  ) {}

}
