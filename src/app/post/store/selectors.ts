import { createSelector } from '@ngrx/store';

import { selectPostFeature } from './state';

export const selectPosts = createSelector(selectPostFeature, state => state.posts);
