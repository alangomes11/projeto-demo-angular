import { Action, createReducer, on } from '@ngrx/store';

import { loadPosts, requestPosts } from './actions';
import { AppPostState, postInitialState } from './state';

const reducers = createReducer(
  postInitialState,
  on(requestPosts, state => ({
    ...state,
    posts: { loading: true }
  })),
  on(loadPosts, (state, { posts }) => ({
    ...state,
    posts: { result: posts, loading: false }
  })),
);

export function postReducers(state: AppPostState | undefined, action: Action) {
  return reducers(state, action);
}
