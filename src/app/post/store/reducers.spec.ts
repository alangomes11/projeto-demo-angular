import { POST_MOCK } from '@test/post.mock';

import { loadPosts, requestPosts } from './actions';
import { postReducers } from './reducers';

describe('Post reducers', () => {

  test('requestPosts action', () => {
    const reduced = postReducers({ prop: 1, posts: { loading: false } } as any, requestPosts({}));

    expect(reduced).toEqual({
      prop: 1,
      posts: { loading: true }
    });
  });

  test('loadPosts action', () => {
    const reduced = postReducers({ prop: 1, posts: { loading: true } } as any, loadPosts({ posts: [POST_MOCK] }));

    expect(reduced).toEqual({
      prop: 1,
      posts: { result: [POST_MOCK], loading: false }
    });
  });

});
