import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { AppState } from '@app/store';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';

import { Post } from './model/post.model';
import { requestPosts, selectPosts } from './store';


@Injectable()
export class PostResolver implements Resolve<Post[]> {

  constructor(private store: Store<AppState>) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Post[]> {
    this.store.dispatch(requestPosts({}));

    return this.store.pipe(
      select(selectPosts),
      first(({ result }) => !!result),
      map(({ result }) => result),
    );
  }

}
