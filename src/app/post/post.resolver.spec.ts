import { TestBed } from '@angular/core/testing';
import { AppState } from '@app/store';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { POST_MOCK } from '@test/post.mock';
import { cold, hot } from 'jest-marbles';

import { PostResolver } from './post.resolver';
import { requestPosts, selectPosts } from './store';


describe('PostResolver', () => {
  let postResolver: PostResolver;
  let store: MockStore<AppState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        provideMockStore({
          selectors: [
            {
              selector: selectPosts,
              value: { result: [POST_MOCK] }
            }
          ]
        }),
        PostResolver,
      ]
    });

    postResolver = TestBed.get(PostResolver);
    store = TestBed.get(Store);
  });

  test('deve disparar requisição para lista de posts', () => {
    const posts$ = postResolver.resolve(null, null);

    expect(store.scannedActions$).toBeObservable(hot('a', {
      a: requestPosts({}),
    }));
    expect(posts$).toBeObservable(cold('(a|)', {
      a: [POST_MOCK],
    }));
  });

});
