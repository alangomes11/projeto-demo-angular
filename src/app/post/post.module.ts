import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared/shared.module';
import { UserModule } from '@app/user/user.module';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { PostContainerComponent } from './post-container/post-container.component';
import { PostListComponent } from './post-list/post-list.component';
import { PostRoutingModule } from './post-routing.module';
import { PostResolver } from './post.resolver';
import { PostEffects, postFeatureKey, postReducers } from './store';


@NgModule({
  declarations: [PostListComponent, PostContainerComponent],
  imports: [
    UserModule,
    CommonModule,
    SharedModule,
    PostRoutingModule,
    EffectsModule.forFeature([PostEffects]),
    StoreModule.forFeature(postFeatureKey, postReducers)
  ],
  providers: [
    PostResolver,
  ]
})
export class PostModule { }
