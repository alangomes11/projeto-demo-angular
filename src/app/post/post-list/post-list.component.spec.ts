import { ActivatedRoute } from '@angular/router';
import { POST_MOCK } from '@test/post.mock';
import { of } from 'rxjs';
import { Shallow } from 'shallow-render';
import { Rendering } from 'shallow-render/dist/lib/models/rendering';

import { PostContainerComponent } from '../post-container/post-container.component';
import { PostModule } from '../post.module';
import { PostListComponent } from './post-list.component';

describe('PostListComponent', () => {
  let render: Rendering<PostListComponent, never>;

  beforeEach(async () => {
    const shallow = new Shallow(PostListComponent, PostModule)
      .provideMock([
        {
          provide: ActivatedRoute,
          useFactory: () => ({ data: of({ posts: [POST_MOCK] }) })
        }
      ]);

    render = await shallow.render();
  });

  test('deve renderizar a lista de posts', () => {
    render.fixture.detectChanges();

    expect(render.fixture).toMatchSnapshot();
    expect(render.findComponent(PostContainerComponent)[0].post).toEqual(POST_MOCK);
  });
});
