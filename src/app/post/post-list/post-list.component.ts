import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { pluck } from 'rxjs/operators';

import { Post } from '../model/post.model';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PostListComponent implements OnInit {

  posts$: Observable<Post[]> = this.activatedRoute.data.pipe(pluck('posts'));

  constructor(
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
  }

  postTrack(index: number, post: Post): number {
    return post.id;
  }

}
