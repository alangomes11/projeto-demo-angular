import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';

import { Post } from './model/post.model';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(
    private httpClient: HttpClient,
  ) { }

  getPosts(query: { userId?: number } = {}): Observable<Post[]> {
    let params = new HttpParams();
    if (query.userId) {
      params = params.set('userId', `${query.userId}`);
    }
    return this.httpClient.get<Post[]>(`${environment.endpoint}/posts`, { params });
  }

  getPost(id: number): Observable<Post> {
    return this.httpClient.get<Post>(`${environment.endpoint}/posts/${id}`);
  }

}
