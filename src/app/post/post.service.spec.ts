import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { environment } from '@env/environment';
import { POST_MOCK } from '@test/post.mock';

import { PostService } from './post.service';

describe('PostService', () => {
  let httpMock: HttpTestingController;
  let service: PostService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PostService],
    });
    httpMock = TestBed.get(HttpTestingController);
    service = TestBed.get(PostService);
  });

  afterEach(() => httpMock.verify());

  test('deve consultar lista de posts', done => {
    service.getPosts().subscribe(res => {
      expect(res).toHaveLength(1);
      done();
    });

    httpMock
      .expectOne(`${environment.endpoint}/posts`)
      .flush([POST_MOCK]);
  });
});
