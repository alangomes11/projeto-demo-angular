import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { AsyncResult } from '@app/model/http.model';
import { AppState } from '@app/store';
import { User } from '@app/user/model/user.model';
import { requestUser, selectUser } from '@app/user/store';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { Post } from '../model/post.model';

@Component({
  selector: 'app-post-container',
  templateUrl: './post-container.component.html',
  styleUrls: ['./post-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PostContainerComponent implements OnInit {

  @Input()
  post: Post;

  user$: Observable<AsyncResult<User>>;

  constructor(
    private store: Store<AppState>,
  ) { }

  ngOnInit() {
    const { userId } = this.post;
    this.store.dispatch(requestUser({ id: userId }));
    this.user$ = this.store.pipe(select(selectUser, userId));
  }

}
