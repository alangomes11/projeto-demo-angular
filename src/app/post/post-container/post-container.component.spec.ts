import { TestBed } from '@angular/core/testing';
import { PostComponent } from '@app/shared/post/post.component';
import { AppState } from '@app/store';
import { requestUser, selectUser } from '@app/user/store';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { POST_MOCK, USER_MOCK } from '@test/post.mock';
import { hot } from 'jest-marbles';
import { Shallow } from 'shallow-render';
import { Rendering } from 'shallow-render/dist/lib/models/rendering';

import { Post } from '../model/post.model';
import { PostModule } from '../post.module';
import { PostContainerComponent } from './post-container.component';

describe('PostComponent', () => {
  let render: Rendering<PostContainerComponent, { post: Post }>;
  let store: MockStore<AppState>;

  beforeEach(async () => {
    const shallow = new Shallow(PostContainerComponent, PostModule)
      .provideMock([provideMockStore({
        selectors: [
          {
            selector: selectUser,
            value: { result: USER_MOCK, loading: false }
          }
        ]
      })]);

    render = await shallow.render({ bind: { post: POST_MOCK } });
    store = TestBed.get(Store);
  });

  test('deve renderizar um post', () => {
    render.fixture.detectChanges();
    render.fixture.detectChanges();

    expect(render.fixture).toMatchSnapshot();
    const postComponent = render.findComponent(PostComponent)[0];
    expect(postComponent.post).toEqual(POST_MOCK);
    expect(postComponent.user).toEqual(USER_MOCK);
  });

  test('deve disparar requisição do usuário autor do post', () => {
    render.fixture.detectChanges();

    expect(store.scannedActions$).toBeObservable(hot('a', {
      a: requestUser({ id: POST_MOCK.userId })
    }));
  });
});
