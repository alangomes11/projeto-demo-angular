import { Post } from '@app/post/model/post.model';
import { User } from '@app/user/model/user.model';

export const POST_MOCK: Post = {
  id: 1,
  userId: 5,
  title: 'post teste',
  body: 'este é um post'
};


export const USER_MOCK: User = {
  id: 5,
  name: 'Bryan',
  username: 'bryan123',
  email: 'bryan@gmail.com'
};
